import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private router: Router) {

    this.loadData();

    // var data:any =localStorage.getItem('data');

    // console.log(JSON.parse(data))
   }


  isLoggedIn(): boolean {

    if(localStorage.getItem('login') =='success'){
      return true;
    }
    else{
      return false;
    }
  }

  logout(){
    localStorage.removeItem('login');
    this.router.navigateByUrl('/login')
  }


  loadData(){

    if(!localStorage.getItem('data')){

    
     var data :any = [
      {id:131043,name:'Karhhik',sections:{first:"0",second:"2",third:"3",fourth:"5"},JoinDate:"09/06/1998 10:41 PM",status:'activr'},
      {id:131045,name:'Karhhik',sections:{first:"0",second:"2",third:"3",fourth:"5"},JoinDate:"09/06/1998 10:41 PM",status:'activr'},
      {id:131046,name:'Karhhik',sections:{first:"0",second:"2",third:"3",fourth:"5"},JoinDate:"09/06/1998 10:41 PM",status:'activr'},
      {id:131047,name:'Karhhik',sections:{first:"0",second:"2",third:"3",fourth:"5"},JoinDate:"09/06/1998 10:41 PM",status:'activr'},
    ]

    localStorage.setItem('data',JSON.stringify(data));
  }
  }




}
