import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatNativeDateModule } from '@angular/material';
// import {MatMomentDateModule} from '@angular/material-moment-adapter';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDialogModule} from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';


import {MatCheckboxModule} from '@angular/material/checkbox';


import {MatToolbarModule} from '@angular/material/toolbar';

import {MatListModule} from '@angular/material/list';



@NgModule({
    imports: [MatCardModule, MatButtonModule, MatInputModule, MatIconModule, MatSelectModule, MatTabsModule,
        MatDividerModule, MatExpansionModule, MatDatepickerModule, MatStepperModule,MatNativeDateModule,MatPaginatorModule,
        MatRadioModule, MatSnackBarModule, MatMenuModule, MatFormFieldModule,MatSidenavModule,MatDialogModule,MatTableModule
        ,MatSortModule,MatCheckboxModule,MatToolbarModule,MatListModule],
    exports: [MatCardModule, MatButtonModule, MatInputModule, MatIconModule, MatSelectModule, MatTabsModule,
        MatDividerModule, MatExpansionModule, MatDatepickerModule, MatStepperModule,MatNativeDateModule,MatPaginatorModule,
        MatRadioModule, MatSnackBarModule, MatMenuModule, MatFormFieldModule,MatSidenavModule,MatDialogModule,MatTableModule,
        MatSortModule,MatCheckboxModule,MatToolbarModule,MatListModule],
})

export class MaterialModule { }