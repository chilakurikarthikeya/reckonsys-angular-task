import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-members',
  templateUrl: './list-members.component.html',
  styleUrls: ['./list-members.component.css']
})
export class ListMembersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.getData()
  }


  Members_list: any;

  getData() {
    let data: any = localStorage.getItem('data');
    this.Members_list = JSON.parse(data)
  }

  edit(id: any) {

    let data: any = localStorage.getItem('data');

  }
  delete(id: any) {


    let data: any = localStorage.getItem('data');
    data = JSON.parse(data)
   
    const filterArray = data.filter((item :any) => item.id !== id);

    localStorage.setItem('data',JSON.stringify(filterArray));
    this.Members_list = filterArray
  

  }
}