import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-create-member',
  templateUrl: './create-member.component.html',
  styleUrls: ['./create-member.component.css']
})
export class CreateMemberComponent implements OnInit {

  constructor(private api:ServiceService,private router: Router) { }

  memberForm = new FormGroup({
    id:new FormControl(this.generateId(),[Validators.required]),
    name:new FormControl('',[Validators.required]),
    sections: new FormGroup({
      first: new FormControl('',[Validators.required]),
      second: new FormControl('',[Validators.required]),
      third: new FormControl('',[Validators.required]),
      fourth: new FormControl('',[Validators.required])
    }),
    JoinDate :new FormControl('',[Validators.required]),
    status:new FormControl('active',[Validators.required])
    
  })
  ngOnInit(): void {
  }


   generateId(){
    return '_' + Math.random().toString(36).substr(2, 9);
   }


  Onsubmit(){

    if(this.memberForm.invalid){
      alert("please provide proper details");
    
    }
    else{
        let data_to_insert = this.memberForm.value;
        console.log(data_to_insert)
         
        let local_data:any =localStorage.getItem('data');
        local_data = JSON.parse(local_data);

        local_data.push(data_to_insert);
        local_data =JSON.stringify(local_data);

        localStorage.setItem('data',local_data);

        alert("Member created");
        this.memberForm.reset();
      
         
    }
  }
}
