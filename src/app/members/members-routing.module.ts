import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateMemberComponent } from './create-member/create-member.component';
import { ListMembersComponent } from './list-members/list-members.component';
import { ProductsComponent } from './products/products.component';
import { ReportingComponent } from './reporting/reporting.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path: 'create_member',component:CreateMemberComponent},
  {path: 'list_members',component:ListMembersComponent},
  {path: 'products',component:ProductsComponent},
  {path: 'reporting',component:ReportingComponent},
  {path: 'users',component:UsersComponent},
  {path: '',component:ListMembersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }
