import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { GuardGuard } from './guards/guard.guard';
import { DashboardComponent } from './members/dashboard/dashboard.component';
import { LoginComponent } from './usermanagement/login/login.component';

const routes: Routes = [
  {path: 'login',component:LoginComponent},
  // {path: '',component:LoginComponent},
  {
    path: '',
    component:DashboardComponent,
    canActivate :[GuardGuard],
    loadChildren:() => import('./members/members.module').then(m=>m.MembersModule),
   
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
