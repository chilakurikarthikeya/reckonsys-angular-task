import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ServiceService } from '../services/service.service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {

  constructor(private api:ServiceService,private router:Router){}
  canActivate():  boolean  {
   
      if(!this.api.isLoggedIn()){
        console.log("hi")
        this.router.navigateByUrl('/login');
        return false;
      }
     

      return true;
    
  }
  
}
