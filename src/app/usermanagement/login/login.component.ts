import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private api:ServiceService,private router: Router) { }

  loginForm = new FormGroup({
    email:new FormControl('',[Validators.required,Validators.email]),
    password:new FormControl('',[Validators.required,Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")])
    
  })

  ngOnInit(): void {

    // checkin login 

    if(this.api.isLoggedIn()){

      this.router.navigateByUrl('/dashboard')
    }
  }

  Onsubmit(){
    // validationg login form
    if(this.loginForm.invalid){
      return alert("Please  verify detail Once")
    }

    if((this.loginForm.value.email =="test@gmail.com") && this.loginForm.value.password=="TEST@ing1" ){
      
       localStorage.setItem('login','success');
       this.router.navigateByUrl('/list_members')
    }
    else{
      alert("Please prove proper details")
    }
  }

}
